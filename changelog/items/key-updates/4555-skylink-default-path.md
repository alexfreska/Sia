- Added `defaultpath` - a new optional path parameter when creating Skylinks. It
  determines which is the default file to open in a multi-file skyfile.
